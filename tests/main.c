#include <stdio.h>
#include "../libutf8.h"

int code_point_outside_range_error() {
    UCS_Code_Point code_point = 0x00110000;
    uint8_t buf[4];

    int result = libutf8_encode_code_point(buf, 4, code_point);

    if (result == LIB_UTF8_INVALID_CODE_POINT) {
        puts("[OK] Detect code point outside range");
        return 0;
    }

    printf("[NOK] Detect code point outside range failed with value 0x00110000. Expected -1, got %d\n", result);
    return -1;
}

int code_point_within_reserved_range() {
    UCS_Code_Point start = 0xD800;
    UCS_Code_Point end = 0xDFFF;
    uint8_t buf[4];
    int result;

    for (; start <= end; start++) {
        result = libutf8_encode_code_point(buf, 4, start);

        if (result != LIB_UTF8_INVALID_CODE_POINT) {
            printf("[NOK] Detect code point inside reserved range failed with value %x. Expected -1, got %d", start, result);
            return -1;
        }
    }

    puts("[OK] Detect code point inside reserved range");
    return 0;
}

int prevent_buffer_overflow() {
    uint8_t buf[1];
    UCS_Code_Point code_point = 0x00010000;

    int result = libutf8_encode_code_point(buf, 1, code_point);

    if (result == LIB_UTF8_BUFFER_OVERFLOW) {
        puts("[OK] Prevent buffer overflow");
        return 0;
    }

    printf("[NOK] Prevent buffer overflow failed. Expected %d, got %d", LIB_UTF8_BUFFER_OVERFLOW, result);
    return -1;
}

int main() {
    int result = 0;
    
    result = result | code_point_outside_range_error();
    result = result | code_point_within_reserved_range();
    result = result | prevent_buffer_overflow();

    return result;
}