/*  
    Copyright (C) 2021  Ruven Salamon

    This file is part of libutf8

    libutf8 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    libutf8 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>. 
*/

#ifndef LIB_UTF8
#define LIB_UTF8

#include<stddef.h>
#include <stdint.h>

#define LIB_UTF8_OK 0
#define LIB_UTF8_INVALID_CODE_POINT -1
#define LIB_UTF8_BUFFER_OVERFLOW -2
#define LIB_UTF8_INVALID_OCTET -3

typedef uint32_t UCS_Code_Point;

int libutf8_encode_code_point(uint8_t* const buf, size_t buf_size, UCS_Code_Point code_point);

#endif
