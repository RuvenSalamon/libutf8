/*
	Copyright (C) 2021  Ruven Salamon

    This file is part of libutf8

    libutf8 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    libutf8 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>. 
*/

#include "./libutf8.h"

int get_num_octets_required(UCS_Code_Point code_point) 
{
	if (code_point <= 0x7F) {
		return 1;
	}

	if (code_point <= 0x07FF) {
		return 2;
	}

	if (code_point <= 0xFFFF) {
		return 3;
	}

	return 4;
}

// Assumes more than 1 octet is required. 1 octet is ascii and doesn't require a mask.
int get_msb_mask(int num_octets) {
	return ((1 << num_octets) - 1) << (8 - num_octets);
}

int validate_code_point(UCS_Code_Point code_point) {
	int outside_range = code_point > 0x0010FFFF;
	int inside_UTF16_reserved_range = code_point >= 0xD800 && code_point <= 0xDFFF;

	if (outside_range || inside_UTF16_reserved_range) {
		return LIB_UTF8_INVALID_CODE_POINT;
	}

	return LIB_UTF8_OK;
}

int validate_octet(uint8_t octet) {
	int invalid = octet == 0xC0 || octet == 0xC1 || (octet >= 0xF5 && octet <= 0xFF);

	if (invalid) {
		return LIB_UTF8_INVALID_OCTET;
	}

	return 0;
}

// TODO: what happens if we add 1 to max int index
uint8_t get_bits(UCS_Code_Point code_point, int num_octets_required, int index) {
	int mask = (1 << 6) - 1;
	int shift_amount = 6 * (num_octets_required - (index + 1));
	return mask & (code_point >> shift_amount);
}

int insert_octet(uint8_t* buf, UCS_Code_Point code_point, int index, int mask, int num_octets_required) {
	uint8_t octet = mask | get_bits(code_point, num_octets_required, index);
	int octet_validation = validate_octet(octet);

	if (octet_validation != LIB_UTF8_OK) {
		return octet_validation;
	}

	buf[index] = octet;

	return LIB_UTF8_OK;
}

int fill_msb(uint8_t* buf, UCS_Code_Point code_point, int num_octets_required) {
	int mask = get_msb_mask(num_octets_required);
	return insert_octet(buf, code_point, 0, mask, num_octets_required);
}

int fill_lsbs(uint8_t* buf, UCS_Code_Point code_point, int num_octets_required) {
	int status = LIB_UTF8_OK;
	
	for (int i = 1; i < num_octets_required; i++) {
		int mask = 0x80;
		status = insert_octet(buf, code_point, i, mask, num_octets_required);

		if (status != LIB_UTF8_OK) {
			break;
		}
	}

	return status;
}

int fill_buffer(uint8_t* buf, size_t buf_size, UCS_Code_Point code_point) {
	int num_octets_required = get_num_octets_required(code_point);

	if (num_octets_required > buf_size) {
		return LIB_UTF8_BUFFER_OVERFLOW;
	}

	if (num_octets_required == 1) {
		buf[0] = (uint8_t) code_point;
		return LIB_UTF8_OK;
	}
	
	int status = fill_msb(buf, code_point, num_octets_required);

	if (status != LIB_UTF8_OK) {
		return status;
	}

	return fill_lsbs(buf, code_point, num_octets_required);
}

int libutf8_encode_code_point(uint8_t* const buf, size_t buf_size, UCS_Code_Point code_point) {
	int code_point_validation = validate_code_point(code_point);

	if (code_point_validation != LIB_UTF8_OK) {
		return code_point_validation;
	}

	int status = fill_buffer(buf, buf_size, code_point);

	return status;
}
