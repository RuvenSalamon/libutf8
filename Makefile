.PHONY: clean cleanall

CC=gcc
CFLAGS=-Wall -pedantic -std=c99

static: libutf8.a clean

libutf8.a: libutf8.o
	ar -cq libutf8.a libutf8.o
	
libutf8.o: libutf8.c
	$(CC) $(CFLAGS) -c libutf8.c

clean:
	$(RM) *.o

cleanall:
	$(RM) *.o *.a
